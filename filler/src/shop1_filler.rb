require './lib/config'
require './lib/db'
require './lib/file_worker'
require './lib/method_detector'
require './filler/src/shop_filler'
require './filler/src/sellers'

class Shop1Filler < ShopFiller
  @db = nil
  @src_db_name = 'shop1'

  @types = {:clothing => 'OD', :haberdashery => 'GA', :footwear => 'OB'}
  @consumers = {:child => '01', :male => '02', :female => '03'}
  @colors = {:white => 'WHITE', :red => 'RED##', :black => 'BLACK',
             :green => 'GREEN', :blue => 'BLUE#'}



  def self.translate_products(normal_prods)
    begin
      shop_prods = Array.new
      normal_prods.each do |normal_prod|
        shop_prod = {
            :article => "#{@types[normal_prod[:type]]}-"+
                        "#{@consumers[normal_prod[:consumer]]}-"+
                        "#{@colors[normal_prod[:color]]}-"+
                        "#{self.generate_token(TOKEN_LENGTH)}",
            :name => normal_prod[:name],
            :price => normal_prod[:price]
        }
        shop_prods << shop_prod
      end
      return shop_prods
    rescue Exception => e
      FileWorker.report_to(ERROR_LOG, "Error in #{self.class.name}.#{MethodDetector.get_method_name}:\n   #{e.message}\n   #{e.backtrace}")
      return nil
    end
  end



  def self.generate_sellings(num)
    begin
      sellings = Array.new
      (1..num).each do
        bill = self.generate_token(TOKEN_LENGTH)
        date = self.generate_date
        seller = @db.execute_query("SELECT id FROM sellers ORDER BY RAND() LIMIT 1").fetch_row[0]
        @db.execute_query("SELECT id FROM products ORDER BY RAND() LIMIT #{rand(1..20)}").each do |row|
          bill_position = Hash.new
          bill_position[:bill] = bill
          bill_position[:product] = row[0]
          bill_position[:seller] = seller
          bill_position[:quantity] = rand(1..10)
          bill_position[:date] = date
          sellings << bill_position
        end
      end
      return sellings
    rescue Exception => e
      FileWorker.report_to(ERROR_LOG, "Error in #{self.class.name}.#{MethodDetector.get_method_name}:\n   #{e.message}\n   #{e.backtrace}")
      return nil
    end
  end



  def self.fill(sellers_num, normal_prods, sellings_num)
    begin
      self.init
      @db.execute_query("DELETE FROM sellings")
      @db.execute_query("DELETE FROM products")
      @db.execute_query("DELETE FROM sellers")

      Sellers.generate(sellers_num).each do |seller|
        @db.execute_query("INSERT INTO sellers(name, gender) "+
                          "VALUES('#{seller[:name]}','#{seller[:gender]}')")
      end

      self.translate_products(normal_prods).each do |product|
        @db.execute_query("INSERT INTO products(article, name, price) "+
                          "VALUES('#{product[:article]}', '#{product[:name]}', "+
                                 "#{product[:price]})")
      end

      self.generate_sellings(sellings_num).each do |bill|
        @db.execute_query("INSERT INTO sellings(bill, product, seller, quantity, date) "+
                          "VALUES('#{bill[:bill]}', #{bill[:product]}, #{bill[:seller]}, "+
                                 "#{bill[:quantity]}, '#{bill[:date]}')")
      end
    rescue Exception => e
      FileWorker.report_to(ERROR_LOG, "Error in #{self.class.name}.#{MethodDetector.get_method_name}:\n   #{e.message}\n   #{e.backtrace}")
      return nil
    end
  end

end
