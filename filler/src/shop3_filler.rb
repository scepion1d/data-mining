require './lib/config'
require './lib/db'
require './lib/file_worker'
require './lib/method_detector'
require './filler/src/shop_filler'
require './filler/src/sellers'

class Shop3Filler < ShopFiller
  @db = nil
  @src_db_name = 'shop3'

  @types = {:clothing => 'clothing', :haberdashery => 'haberdashery', :footwear => 'footwear'}
  @consumers = {:male => 'M', :female => 'F', :child => 'K'}
  @colors = {:white => 'white', :red => 'red', :black => 'black',
             :green => 'green', :blue => 'blue'}



  def self.translate_products(normal_prods)
    begin
      shop_prods = {'clothing' => Array.new, 'haberdashery' => Array.new, 'footwear' => Array.new}
      normal_prods.each do |normal_prod|
        type = @types[normal_prod[:type]]
        shop_prod = {
            :name => normal_prod[:name],
            :consumer => @consumers[normal_prod[:consumer]],
            :color => @colors[normal_prod[:color]],
            :price => normal_prod[:price]
        }
        shop_prods[type] << shop_prod
      end
      return shop_prods
    rescue Exception => e
      FileWorker.report_to(ERROR_LOG, "Error in #{self.class.name}.#{MethodDetector.get_method_name}:\n   #{e.message}\n   #{e.backtrace}")
      return nil
    end
  end



  def self.generate_bills(num)
    begin
      bills = Array.new
      num.times do
        bill = {
            :name => self.generate_token(TOKEN_LENGTH),
            :date => self.generate_date
        }
        bills << bill
      end
      return bills
    rescue
      FileWorker.report_to(ERROR_LOG, "Error in #{self.class.name}.#{MethodDetector.get_method_name}:\n   #{e.message}\n   #{e.backtrace}")
      return nil
    end
  end



  def self.add_to_bill(sellings, position)
    #puts "#{sellings}\n\n#{position}\n\n"
    (0..sellings.size-1).each do |i|
      if sellings[i][:bill] == position[:bill]
        if sellings[i][:product] == position[:product]
          return sellings
        end
      end
    end
    sellings << position
    #puts "#{sellings}\n\n\n\n\n"
    return sellings
  end



  def self.generate_sellings(prods)
    begin
      sellings = {'clothing' => Array.new, 'haberdashery' => Array.new, 'footwear' => Array.new}
      @db.execute_query("SELECT id FROM bills").each do |row|
        prod_num = rand(1..20)
        bill = row[0]
        seller = @db.execute_query("SELECT id FROM sellers ORDER BY RAND() LIMIT 1").fetch_row[0]
        prod_num.times do
          type = @types[prods[rand(0..prods.size-1)][:type]]
          product = @db.execute_query("SELECT id FROM #{type} ORDER BY RAND() LIMIT 1").fetch_row[0]
          bill_position = {
              :bill => bill,
              :product => product,
              :seller => seller,
              :quantity => rand(1..10)
          }
          sellings[type] = self.add_to_bill(sellings[type], bill_position)
        end
      end
      return sellings
    rescue Exception => e
      FileWorker.report_to(ERROR_LOG, "Error in #{self.class.name}.#{MethodDetector.get_method_name}:\n   #{e.message}\n   #{e.backtrace}")
      return nil
    end
  end



  def self.fill(sellers_num, normal_prods, sellings_num)
    begin
      self.init
      @types.each do |key, type|
        @db.execute_query("DELETE FROM #{type}_sellings")
        @db.execute_query("DELETE FROM #{type}")
      end
      @db.execute_query("DELETE FROM bills")
      @db.execute_query("DELETE FROM sellers")

      Sellers.generate(sellers_num).each do |seller|
        @db.execute_query("INSERT INTO sellers(name, gender) "+
                          "VALUES('#{seller[:name]}','#{seller[:gender]}')")
      end

      self.translate_products(normal_prods).each do |type, prods|
        prods.each do |prod|
          @db.execute_query("INSERT INTO #{type}(name, color, consumer, price) " +
                            "VALUES('#{prod[:name]}', '#{prod[:color]}', " +
                                   "'#{prod[:consumer]}', #{prod[:price]})")
        end
      end

      self.generate_bills(sellings_num).each do |bill|
        @db.execute_query("INSERT INTO bills(name, date) " +
                          "VALUES('#{bill[:name]}', '#{bill[:date]}')")
      end

      self.generate_sellings(normal_prods).each do |type, bills|
        bills.each do |bill|
          #puts "#{type} : #{bill}"
          @db.execute_query("INSERT INTO #{type}_sellings(bill, product, seller, quantity) "+
                            "VALUES(#{bill[:bill]}, #{bill[:product]}, #{bill[:seller]}, #{bill[:quantity]})")
        end
      end
    rescue Exception => e
      FileWorker.report_to(ERROR_LOG, "Error in #{self.class.name}.#{MethodDetector.get_method_name}:\n   #{e.message}\n   #{e.backtrace}")
      return nil
    end
  end

end
