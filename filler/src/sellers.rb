require './lib/config'
require './lib/file_worker'
require './lib/method_detector'

class Sellers

  def self.generate_gender
    return :M if rand(0..1) > 0
    return :F
  end

  def self.generate_name(gender)
    fn_id = rand(0..FIRST_NAMES[gender].size-1) # first name id
    ln_id = rand(0..LAST_NAMES.size-1) # last name id
    return "#{FIRST_NAMES[gender][fn_id]} #{LAST_NAMES[ln_id]}"
  end

  def self.generate(num)
    begin
      sellers = Array.new
      num.times do
        seller = Hash.new
        seller[:gender] = generate_gender
        seller[:name] = generate_name(seller[:gender])
        sellers << seller
      end
      return sellers
    rescue Exception => e
      FileWorker.report_to(ERROR_LOG, "Error in #{self.class.name}.#{MethodDetector.get_method_name}:\n   #{e.message}\n   #{e.backtrace}")
      return nil
    end
  end

end