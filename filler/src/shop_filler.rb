require './lib/config'
require './lib/file_worker'
require './lib/method_detector'
require 'securerandom'

class ShopFiller
  @src_db_name = nil

  def self.generate_token(length = 20)
    return SecureRandom.base64(length).gsub("/","_").gsub(/=+$/,"")
  end

  def self.generate_date
    return "#{rand(2000..Time.now.year)}-#{rand(1..12)}-#{rand(1..28)}"
  end

  def self.init
    begin
      if @db.nil?
        @db = DB.new
        @db.connect(HOST, USER, PASS, @src_db_name)
      end
    rescue Exception => e
      FileWorker.report_to(ERROR_LOG, "Error in #{self.class.name}.#{MethodDetector.get_method_name}:\n   #{e.message}\n   #{e.backtrace}")
      return nil
    end
  end

  def self.destruct
    begin
      @db.disconnect
      @db = nil
    rescue Exception => e
      FileWorker.report_to(ERROR_LOG, "Error in #{self.class.name}.#{MethodDetector.get_method_name}:\n   #{e.message}\n   #{e.backtrace}")
      return nil
    end
  end

end