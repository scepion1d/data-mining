require './lib/config'
require './lib/db'
require './lib/file_worker'
require './lib/method_detector'
require './filler/src/shop_filler'
require './filler/src/sellers'
require 'set'

class Shop2Filler < ShopFiller
  @db = nil
  @src_db_name = 'shop2'

  @types = {:clothing => 'clothing', :haberdashery => 'haberdashery', :footwear => 'footwear'}
  @consumers = {:male => 'M', :female => 'F', :child => 'C'}
  @colors = {:white => 'white', :red => 'red', :black => 'black',
             :green => 'green', :blue => 'blue'}



  def self.translate_products(normal_prods)
    begin
      shop_prods = Array.new
      normal_prods.each do |normal_prod|
        shop_prod = {
            :name => normal_prod[:name],
            :type => @types[normal_prod[:type]],
            :consumer => @consumers[normal_prod[:consumer]],
            :color => @colors[normal_prod[:color]],
            :price => normal_prod[:price]
        }
        shop_prods << shop_prod
      end
      return shop_prods
    rescue Exception => e
      FileWorker.report_to(ERROR_LOG, "Error in #{self.class.name}.#{MethodDetector.get_method_name}:\n   #{e.message}\n   #{e.backtrace}")
      return nil
    end
  end



  def self.rand_n(n, max)
    randoms = Set.new
    loop do
      randoms << rand(max)
      return randoms.to_a if randoms.size >= n
    end
  end



  def self.generate_sellings(num, prods)
    begin
      sellings = Array.new
      (1..num).each do
        bill = self.generate_token(TOKEN_LENGTH)
        date = self.generate_date
        seller = @db.execute_query("SELECT id FROM sellers ORDER BY RAND() LIMIT 1").fetch_row[0]
        self.rand_n(rand(1..20), prods.size).each do |id|
          bill_position = {
              :bill => bill,
              :name => prods[id][:name],
              :type => prods[id][:type],
              :consumer => prods[id][:consumer],
              :color => prods[id][:color],
              :price => prods[id][:price],
              :seller => seller,
              :quantity => rand(1..10),
              :date => date
          }
          sellings << bill_position
        end
      end
      return sellings
    rescue Exception => e
      FileWorker.report_to(ERROR_LOG, "Error in #{self.class.name}.#{MethodDetector.get_method_name}:\n   #{e.message}\n   #{e.backtrace}")
      return nil
    end
  end



  def self.fill(sellers_num, normal_prods, sellings_num)
    begin
      self.init
      @db.execute_query("DELETE FROM sellings")
      @db.execute_query("DELETE FROM sellers")

      Sellers.generate(sellers_num).each do |seller|
        @db.execute_query("INSERT INTO sellers(name, gender) "+
                          "VALUES('#{seller[:name]}','#{seller[:gender]}')")
      end

      self.generate_sellings(sellings_num, self.translate_products(normal_prods)).each do |bill|
        @db.execute_query("INSERT INTO sellings(bill, prod_name, "+
                              "prod_type, prod_consumer, prod_color, "+
                              "prod_price, seller, quantity, date) "+
                          "VALUES('#{bill[:bill]}', '#{bill[:name]}', "+
                              "'#{bill[:type]}', '#{bill[:consumer]}', "+
                              "'#{bill[:color]}', '#{bill[:price]}', "+
                              "'#{bill[:seller]}','#{bill[:quantity]}','#{bill[:date]}')")
      end
    rescue Exception => e
      FileWorker.report_to(ERROR_LOG, "Error in #{self.class.name}.#{MethodDetector.get_method_name}:\n   #{e.message}\n   #{e.backtrace}")
      return nil
    end
  end

end
