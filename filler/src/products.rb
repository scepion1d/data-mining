require './lib/config'
require './lib/file_worker'
require './lib/method_detector'
require './lib/db'

class Products
  @types = [:clothing, :haberdashery, :footwear]
  @consumers = [:male, :female, :child]
  @colors = [:white, :red, :black, :green, :blue]

  @db = nil
  @db_name = 'products'

  def self.generate_price(min, max)
    return rand(min..max)
  end

  def self.generate(number)
    begin
      init
      products = Array.new
      (1..number).each do
        product = Hash.new
        product[:type]  = @types[rand(0..@types.size-1)]
        product[:consumer]  = @consumers[rand(0..@consumers.size-1)]
        product[:color] = @colors[rand(0..@colors.size-1)]
        product[:name]  = PRODUCTS_NAMES[product[:type]][rand(0..PRODUCTS_NAMES[product[:type]].size-1)]
        product[:price] = self.generate_price(100, 10000)
        id = find_entry(
            'products',
            [
                {:column => 'name', :value => product[:name]},
                {:column => 'type', :value => product[:type]},
                {:column => 'consumer', :value => product[:consumer]},
                {:column => 'color', :value => product[:color]}
            ]
        );
        if id.nil?
          create_entry(
              'products',
              [
                  {:column => 'name', :value => product[:name]},
                  {:column => 'type', :value => product[:type]},
                  {:column => 'consumer', :value => product[:consumer]},
                  {:column => 'color', :value => product[:color]},
                  {:column => 'price', :value => product[:price]}
              ]
          );
        end
      end
      destruct
      return extract_products
    rescue Exception => e
      FileWorker.report_to(ERROR_LOG, "Error in #{self.class.name}.#{MethodDetector.get_method_name}:\n   #{e.message}\n   #{e.backtrace}")
      return nil
    end
  end

  def self.extract_products()
    begin
      init
      products = Array.new
      query = "SELECT * FROM #{@db_name}.products"
      table = @db.execute_query(query)
      table.each do |row|
        product = {
            :name => row[1].to_s,
            :type => row[2].to_sym,
            :consumer => row[3].to_sym,
            :color => row[4].to_sym,
            :price => row[5].to_i
        }
        products << product
      end
      destruct
      return products
    rescue Exception => e
      FileWorker.report_to(ERROR_LOG, "Error in #{self.class.name}.#{MethodDetector.get_method_name}:\n   #{e.message}\n   #{e.backtrace}")
      return nil
    end
  end

  def self.build_select_query(table, params)
    query = "SELECT id FROM #{@db_name}.#{table} WHERE "

    params.each_with_index do |param, i|
      query += "#{param[:column]}="
      param[:value].class.name == Fixnum.name ?
          query += "#{param[:value]} " :
          query += "'#{param[:value]}' "
      query += "and " if i < params.size-1
    end

    return query
  end


  def self.build_insert_query(table, params)
    query = "INSERT INTO #{@db_name}.#{table}("

    params.each_with_index do |param, i|
      query += "#{param[:column]}"
      query += ", " if i < params.size-1
    end
    query += ") VALUES("

    params.each_with_index do |param, i|
      param[:value].class.name == Fixnum.name ?
          query += "#{param[:value]}" :
          query += "'#{param[:value]}'"
      query += ", " if i < params.size-1
    end
    query += ")"

    return query
  end


  def self.find_entry(table, params)
    begin
      query = self.build_select_query(table, params)
      row = @db.execute_query(query).fetch_row

      return nil if row.nil?
      return row[0]
    rescue Exception => e
      FileWorker.report_to(ERROR_LOG, "Error in #{self.class.name}.#{MethodDetector.get_method_name}:\n   #{e.message}\n   #{e.backtrace}")
      return nil
    end
  end


  def self.create_entry(table, params)
    begin
      query = self.build_insert_query(table, params)
      @db.execute_query(query)
    rescue Exception => e
      FileWorker.report_to(ERROR_LOG, "Error in #{self.class.name}.#{MethodDetector.get_method_name}:\n   #{e.message}\n   #{e.backtrace}")
      return nil
    end
  end


  def self.find_or_create_entry(table, params)
    begin
      id = self.find_entry(table, params)
      if id.nil?
        self.create_entry(table, params)
        id = self.find_entry(table, params)
      end
      return id.to_i
    rescue Exception => e
      FileWorker.report_to(ERROR_LOG, "Error in #{self.class.name}.#{MethodDetector.get_method_name}:\n   #{e.message}\n   #{e.backtrace}")
      return nil
    end
  end


  def self.init
    begin
      if @db.nil?
        @db = DB.new
        @db.connect(HOST, USER, PASS, @db_name)
      end
    rescue Exception => e
      FileWorker.report_to(ERROR_LOG, "Error in #{self.class.name}.#{MethodDetector.get_method_name}:\n   #{e.message}\n   #{e.backtrace}")
      return nil
    end
  end


  def self.destruct
    begin
      @db.disconnect
      @db = nil
    rescue Exception => e
      FileWorker.report_to(ERROR_LOG, "Error in #{self.class.name}.#{MethodDetector.get_method_name}:\n   #{e.message}\n   #{e.backtrace}")
      return nil
    end
  end

end