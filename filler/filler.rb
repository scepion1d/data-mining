require './filler/src/products'
require './filler/src/shop1_filler'
require './filler/src/shop2_filler'
require './filler/src/shop3_filler'

puts "Building sources:"
print "    Generating products ... "
products = Products.generate(50)
puts "DONE\n\n"

print "    Filling first source ... "
Shop1Filler.fill(rand(5..15), products, rand(300..400))
puts "DONE\n\n"

print "    Filling second source ... "
Shop2Filler.fill(rand(5..15), products, rand(300..400))
puts "DONE\n\n"

print "    Filling third source ... "
Shop3Filler.fill(rand(5..15), products, rand(300..400))
puts "DONE"
