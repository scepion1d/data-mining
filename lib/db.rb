require './lib/file_worker'
require './lib/method_detector'
require 'mysql'

class DB
  @db = nil

  def connect(host, user, password, db)
    begin
      host = 'localhost' if host.nil?
      @db = Mysql.connect(host, user, password, db)
    rescue Exception => e
      FileWorker.report_to(ERROR_LOG, "Error in #{self.class.name}.#{MethodDetector.get_method_name}:\n   #{e.message}\n   #{e.backtrace}")
      return nil
    end
  end

  def execute_query(query)
    begin
      @db.query(query)
    rescue Exception => e
      FileWorker.report_to(ERROR_LOG, "Error in #{self.class.name}.#{MethodDetector.get_method_name}:\n   #{e.message}\n   #{e.backtrace}")
      return nil
    end
  end

  def disconnect
    begin
      @db.close
      @db = nil
    rescue Exception => e
      FileWorker.report_to(ERROR_LOG, "Error in #{self.class.name}.#{MethodDetector.get_method_name}:\n   #{e.message}\n   #{e.backtrace}")
      return nil
    end
  end

end