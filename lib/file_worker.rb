require 'digest/sha2'
require './lib/method_detector'

class FileWorker

  def self.read_from(path)
    begin
      file = File.open(path, "rb")
      file.read.to_s
    rescue Exception => e
      FileWorker.report_to(ERROR_LOG, "Error in #{self.name}.#{MethodDetector.get_method_name}:\n   #{e.backtrace}")
      return nil
    end
  end


  def self.write_to(file, message)
    begin
      File.open(file, "w") {|f| f.write("#{message}")}
    rescue Exception => e
      FileWorker.report_to(ERROR_LOG, "Error in #{self.name}.#{MethodDetector.get_method_name}:\n   #{e.backtrace}")
      return nil
    end
  end


  def self.append_to(file, message)
    begin
      File.open(file, "a") {|f| f.write("#{message}")}
    rescue Exception => e
      FileWorker.report_to(ERROR_LOG, "Error in #{self.name}.#{MethodDetector.get_method_name}:\n   #{e.backtrace}")
      return nil
    end
  end


  def self.report_to(log, message)
    begin
      self.append_to(log, "#{Time.new.getlocal}\n#{message}\n\n")
    rescue Exception => e
      FileWorker.report_to(ERROR_LOG, "Error in #{self.name}.#{MethodDetector.get_method_name}:\n   #{e.backtrace}")
      return nil
    end
  end


  def self.require_all(path)
    begin
      Dir[File.join(path, "*.rb")].each do |f|
        require f
      end
    rescue Exception => e
      FileWorker.report_to(ERROR_LOG, "Error in #{self.name}.#{MethodDetector.get_method_name}:\n   #{e.backtrace}")
      return nil
    end
  end


  def self.get_dir_checksum(path)
    begin
      files = Dir["#{path}/**/*"].reject{|f| File.directory?(f)}
      content = files.map{|f| File.read(f)}.join
      checksum = Digest::SHA2.new << content
      return checksum .to_s
    rescue Exception => e
      FileWorker.report_to(ERROR_LOG, "Error in #{self.name}.#{MethodDetector.get_method_name}:\n   #{e.backtrace}")
      return nil
    end
  end


  def self.get_files_list(path)
    begin
      files = Array[]
      Dir.foreach(path) do |file|
        if file == file.scan(/.+\.xml?/i)[0]
          files << file
        end
      end
      return files
    rescue Exception => e
      FileWorker.report_to(ERROR_LOG, "Error in #{self.name}.#{MethodDetector.get_method_name}:\n   #{e.backtrace}")
      return nil
    end
  end

end