HOST = 'localhost'
USER = 'expert'
PASS = 'expert'

TOKEN_LENGTH = 20

APPLICATION_DIR = File.absolute_path('.')
LOGS_DIR = APPLICATION_DIR + "/logs"
ERROR_LOG = "#{LOGS_DIR}/error_log"

#NAMES
FIRST_NAMES = {
    :M => ['Jacob', 'Ethan', 'Michael', 'Jayden', 'William', 'Alexander',
           'Noah', 'Daniel', 'Aiden', 'Anthony', 'Joshua', 'Mason',
           'Christopher', 'Andrew', 'David', 'Matthew', 'Logan', 'Elijah',
           'James', 'Joseph', 'Gabriel', 'Benjamin', 'Ryan', 'Samuel',
           'Jackson', 'John', 'Nathan', 'Jonathan', 'Christian', 'Liam'],

    :F => ['Isabella', 'Sophia', 'Emma', 'Olivia', 'Ava', 'Emily',
           'Abigail', 'Madison', 'Chloe', 'Mia', 'Addison', 'Elizabeth',
           'Ella', 'Natalie', 'Samantha', 'Alexis', 'Lily', 'Grace',
           'Hailey', 'Alyssa', 'Lillian', 'Hannah', 'Avery', 'Leah',
           'Nevaeh', 'Sofia', 'Ashley', 'Anna', 'Brianna', 'Sarah']
}

LAST_NAMES = ['Smith', 'Johnson', 'Williams', 'Brown', 'Jones', 'Miller',
              'Davis', 'Garcia', 'Rodriguez', 'Wilson', 'Martinez', 'Anderson',
              'Taylor', 'Thomas', 'Hernandez', 'Moore', 'Martin', 'Jackson',
              'Thompson', 'White', 'Lopez',  'Lee', 'Gonzalez', 'Harris',
              'Clark', 'Lewis', 'Robinson', 'Walker', 'Perez', 'Hall',
              'Young', 'Allen', 'Sanchez', 'Wright', 'King', 'Scott',
              'Green', 'Baker', 'Adams', 'Nelson', 'Hill', 'Ramirez',
              'Campbell', 'Mitchell', 'Roberts', 'Carter', 'Phillips', 'Evans',
              'Turner', 'Torres', 'Parker', 'Fisher']

PRODUCTS_NAMES = {
    :clothing => ['shirt', 't-shirt', 'coat', 'sweater', 'jeans'],
    :haberdashery => ['tie', 'threads', 'gloves', 'scarf', 'bag'],
    :footwear => ['sneakers', 'shoes', 'boots', 'gumshoes']
}