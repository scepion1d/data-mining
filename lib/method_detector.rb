module MethodDetector
  def self.get_method_name
    begin
      caller[0]=~/`(.*?)'/
      $1
    rescue Exception => e
      FileWorker.report_to(ERROR_LOG, "Error in #{self.name}.#{MethodDetector.get_method_name}:\n   #{e.backtrace}")
      return nil
    end
  end
end