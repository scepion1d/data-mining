require './lib/config'
require './lib/db'
require './lib/file_worker'
require './lib/method_detector'

class Pusher
  @db = nil
  @db_name = 'warehouse'

  # hash format:
  # {
  #   :type => String, # "clothing", "footwear", "haberdashery"
  #   :name => String,
  #   :consumer =>  String, # "male" || "female" || "child"
  #   :color => String, # "white" || "red" || "black" || "green" || "blue"
  #   :day => Integer,
  #   :month => Integer,
  #   :year => Integer,
  #   :bill => String, # any token. example: "xmjsvnn2"
  #   :quantity => Integer,
  #   :price => Integer
  # }
  def self.push_data_to_db(data, shop_name)
    begin
      self.init
      data.each do |entry|
        product = self.find_or_create_entry(
            'product_dim',
            [
              {:column => 'name', :value => entry[:name]},
              {:column => 'type', :value => entry[:type]},
              {:column => 'color', :value => entry[:color]},
              {:column => 'price', :value => entry[:price]},
              {:column => 'consumer', :value => entry[:consumer]}
            ]
        )
        shop = self.find_or_create_entry(
            'shop_dim',
            [
                {:column => 'name', :value => shop_name}
            ]
        )
        bill = self.find_or_create_entry(
                    'bills',
                    [
                        {:column => 'code', :value => entry[:bill]}
                    ]
        )
        date = self.find_or_create_entry(
                    'date_dim',
                    [
                        {:column => 'day', :value => entry[:day]},
                        {:column => 'month', :value => entry[:month]},
                        {:column => 'year', :value => entry[:year]}
                    ]
        )

        self.find_or_create_entry(
            'facts',
            [
                {:column => 'bill', :value => bill},
                {:column => 'product', :value => product},
                {:column => 'shop', :value => shop},
                {:column => 'date', :value => date},
                {:column => 'quantity', :value => entry[:quantity]}
            ]
        )
      end
      self.destruct
    rescue Exception => e
      FileWorker.report_to(ERROR_LOG, "Error in #{self.class.name}.#{MethodDetector.get_method_name}:\n   #{e.message}\n   #{e.backtrace}")
      return nil
    end
  end


  def self.build_select_query(table, params)
    query = "SELECT id FROM #{@db_name}.#{table} WHERE "

    params.each_with_index do |param, i|
      query += "#{param[:column]}="
      param[:value].class.name == Fixnum.name ?
          query += "#{param[:value]} " :
          query += "'#{param[:value]}' "
      query += "and " if i < params.size-1
    end

    return query
  end


  def self.build_insert_query(table, params)
    query = "INSERT INTO #{@db_name}.#{table}("

    params.each_with_index do |param, i|
      query += "#{param[:column]}"
      query += ", " if i < params.size-1
    end
    query += ") VALUES("

    params.each_with_index do |param, i|
      param[:value].class.name == Fixnum.name ?
          query += "#{param[:value]}" :
          query += "'#{param[:value]}'"
      query += ", " if i < params.size-1
    end
    query += ")"

    return query
  end


  def self.find_entry(table, params)
    begin
      query = self.build_select_query(table, params)
      row = @db.execute_query(query).fetch_row
        
      return nil if row.nil?
      return row[0]
    rescue Exception => e
      FileWorker.report_to(ERROR_LOG, "Error in #{self.class.name}.#{MethodDetector.get_method_name}:\n   #{e.message}\n   #{e.backtrace}")
      return nil
    end
  end


  def self.create_entry(table, params)
    begin
      query = self.build_insert_query(table, params)
      @db.execute_query(query)
    rescue Exception => e
      FileWorker.report_to(ERROR_LOG, "Error in #{self.class.name}.#{MethodDetector.get_method_name}:\n   #{e.message}\n   #{e.backtrace}")
      return nil
    end
  end


  def self.find_or_create_entry(table, params)
    begin
      id = self.find_entry(table, params)
      if id.nil?
        self.create_entry(table, params)
        id = self.find_entry(table, params)
      end
      return id.to_i
    rescue Exception => e
      FileWorker.report_to(ERROR_LOG, "Error in #{self.class.name}.#{MethodDetector.get_method_name}:\n   #{e.message}\n   #{e.backtrace}")
      return nil
    end
  end


  def self.init
    begin
      if @db.nil?
        @db = DB.new
        @db.connect(HOST, USER, PASS, @db_name)
      end
    rescue Exception => e
      FileWorker.report_to(ERROR_LOG, "Error in #{self.class.name}.#{MethodDetector.get_method_name}:\n   #{e.message}\n   #{e.backtrace}")
      return nil
    end
  end


  def self.destruct
    begin
      @db.disconnect
      @db = nil
    rescue Exception => e
      FileWorker.report_to(ERROR_LOG, "Error in #{self.class.name}.#{MethodDetector.get_method_name}:\n   #{e.message}\n   #{e.backtrace}")
      return nil
    end
  end

end
