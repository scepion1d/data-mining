require './lib/config'
require './lib/db'
require './lib/file_worker'
require './lib/method_detector'
require './extractor/src/shop_extractor'

require './extractor/src/pusher'

class Shop1Extractor < ShopExtractor
  @types = {'GA' => 'haberdashery', 'OD' => 'clothing', 'OB' => 'footwear'}
  @consumers = {'01' => 'child', '02' => 'male', '03' => 'female'}
  @colors = {'WHITE' => 'white', 'RED##' => 'red', 'BLACK' => 'black',
             'GREEN' => 'green', 'BLUE#' => 'blue'}

  @src_db_name = 'shop1'
  @view_name = 'shop1'

  def self.translate_data(data)
    begin
      facts = Array.new
      data.each do |row|
        prod = row[0].split('-')
        date = row[2].split('-')
        fact = {
            :type => @types[prod[0]],
            :name => row[4],
            :consumer => @consumers[prod[1]],
            :color => @colors[prod[2]],
            :day => date[2].to_i,
            :month => date[1].to_i,
            :year => date[0].to_i,
            :bill => row[1],
            :quantity => row[3].to_i,
            :price => row[5].to_i
        }
        facts << fact
      end
      return facts
    rescue Exception => e
      FileWorker.report_to(ERROR_LOG, "Error in #{self.class.name}.#{MethodDetector.get_method_name}:\n   #{e.message}\n   #{e.backtrace}")
      return nil
    end
  end

end