require './lib/config'
require './lib/db'
require './lib/file_worker'
require './lib/method_detector'

class ShopExtractor
  @db = nil
  @src_db_name = nil
  @view_name = nil

  def self.get_data_from_db
    begin
      self.init
      query_result = @db.execute_query("SELECT * FROM #{@src_db_name}.#{@view_name} ORDER BY bill")
      self.destruct
      return query_result
    rescue Exception => e
      FileWorker.report_to(ERROR_LOG, "Error in #{self.class.name}.#{MethodDetector.get_method_name}:\n   #{e.message}\n   #{e.backtrace}")
      return nil
    end
  end

  def self.translate_data(data)
  end

  def self.init
    begin
      if @db.nil?
        @db = DB.new
        @db.connect(HOST, USER, PASS, @src_db_name)
      end
    rescue Exception => e
      FileWorker.report_to(ERROR_LOG, "Error in #{self.class.name}.#{MethodDetector.get_method_name}:\n   #{e.message}\n   #{e.backtrace}")
      return nil
    end
  end

  def self.destruct
    begin
      @db.disconnect
      @db = nil
    rescue Exception => e
      FileWorker.report_to(ERROR_LOG, "Error in #{self.class.name}.#{MethodDetector.get_method_name}:\n   #{e.message}\n   #{e.backtrace}")
      return nil
    end
  end

end
