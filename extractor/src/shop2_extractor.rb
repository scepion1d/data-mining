require './lib/config'
require './lib/db'
require './lib/file_worker'
require './lib/method_detector'
require './extractor/src/shop_extractor'

class Shop2Extractor < ShopExtractor
  @types = {'haberdashery' => 'haberdashery', 'clothing' => 'clothing', 'footwear' => 'footwear'}
  @consumers = {'C' => 'child', 'M' => 'male', 'F' => 'female'}
  @colors = {'white' => 'white', 'red' => 'red', 'black' => 'black',
             'green' => 'green', 'blue' => 'blue'}

  @src_db_name = 'shop2'
  @view_name = 'shop2'

  def self.translate_data(data)
    begin
      facts = Array.new
      data.each do |row|
        date = row[4].split('-')
        fact = {
            :type => @types[row[0]],
            :name => row[6],
            :consumer => @consumers[row[1]],
            :color => @colors[row[2]],
            :day => date[2].to_i,
            :month => date[1].to_i,
            :year => date[0].to_i,
            :bill => row[3],
            :quantity => row[5].to_i,
            :price => row[7].to_i
        }
        facts << fact
      end
      return facts
    rescue Exception => e
      FileWorker.report_to(ERROR_LOG, "Error in #{self.class.name}.#{MethodDetector.get_method_name}:\n   #{e.message}\n   #{e.backtrace}")
      return nil
    end
  end
end
