require './extractor/src/pusher'
require './extractor/src/shop1_extractor'
require './extractor/src/shop2_extractor'
require './extractor/src/shop3_extractor'

puts "Building warehouse:"
print "    Extracting data from first source ... "
data = Shop1Extractor.translate_data(Shop1Extractor.get_data_from_db)
puts "DONE"
print "    Pushing data from first source do warehouse ... "
Pusher.push_data_to_db(data, 'shop1')
puts "DONE\n\n"

print "    Extracting data from second source ... "
data = Shop2Extractor.translate_data(Shop2Extractor.get_data_from_db)
puts "DONE"
print "    Pushing data from second source do warehouse ... "
Pusher.push_data_to_db(data, 'shop2')
puts "DONE\n\n"

print "    Extracting data from third source ... "
data = Shop3Extractor.translate_data(Shop3Extractor.get_data_from_db)
puts "DONE"
print "    Pushing data from third source do warehouse ... "
Pusher.push_data_to_db(data, 'shop3')
puts "DONE"