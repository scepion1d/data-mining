CREATE TABLE 'shop_dim' (
  'id' int(11) NOT NULL AUTO_INCREMENT,
  'name' varchar(45) NOT NULL,
  PRIMARY KEY ('id')
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE 'product_dim' (
  'id' int(11) NOT NULL AUTO_INCREMENT,
  'type' varchar(45) NOT NULL,
  PRIMARY KEY ('id')
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE 'date_dim' (
  'id' int(11) NOT NULL AUTO_INCREMENT,
  'day' int(11) NOT NULL,
  'month' int(11) NOT NULL,
  'year' int(11) NOT NULL,
  PRIMARY KEY ('id')
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE 'consumer_dim' (
  'id' int(11) NOT NULL AUTO_INCREMENT,
  'type' varchar(45) NOT NULL,
  PRIMARY KEY ('id')
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE 'color_dim' (
  'id' int(11) NOT NULL AUTO_INCREMENT,
  'name' varchar(45) NOT NULL,
  PRIMARY KEY ('id')
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE 'bills' (
  'id' int(11) NOT NULL AUTO_INCREMENT,
  'code' varchar(45) NOT NULL,
  PRIMARY KEY ('id')
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE  TABLE IF NOT EXISTS 'warehouse'.'facts' (
  'bill' INT NOT NULL ,
  'product' INT NOT NULL ,
  'color' INT NOT NULL ,
  'consumer' INT NOT NULL ,
  'shop' INT NOT NULL ,
  'date' INT NOT NULL ,
  INDEX 'fk_facts_bills' ('bill' ASC) ,
  INDEX 'fk_facts_product_dim1' ('product' ASC) ,
  INDEX 'fk_facts_color_dim1' ('color' ASC) ,
  INDEX 'fk_facts_consumer_dim1' ('consumer' ASC) ,
  INDEX 'fk_facts_shop_dim1' ('shop' ASC) ,
  INDEX 'fk_facts_date_dim1' ('date' ASC) ,
  CONSTRAINT 'fk_facts_bills'
    FOREIGN KEY ('bill' )
    REFERENCES 'warehouse'.'bills' ('id' )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT 'fk_facts_product_dim1'
    FOREIGN KEY ('product' )
    REFERENCES 'warehouse'.'product_dim' ('id' )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT 'fk_facts_color_dim1'
    FOREIGN KEY ('color' )
    REFERENCES 'warehouse'.'color_dim' ('id' )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT 'fk_facts_consumer_dim1'
    FOREIGN KEY ('consumer' )
    REFERENCES 'warehouse'.'consumer_dim' ('id' )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT 'fk_facts_shop_dim1'
    FOREIGN KEY ('shop' )
    REFERENCES 'warehouse'.'shop_dim' ('id' )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT 'fk_facts_date_dim1'
    FOREIGN KEY ('date' )
    REFERENCES 'warehouse'.'date_dim' ('id' )
    ON DELETE CASCADE
    ON UPDATE CASCADE
) ENGINE = InnoDB DEFAULT CHARACTER SET = utf8;

CREATE ALGORITHM=UNDEFINED DEFINER=`expert`@`localhost` SQL SECURITY DEFINER VIEW `warehouse`.`warehouse` AS 
select 
	`warehouse`.`product_dim`.`name` AS `Product Name`,
	`warehouse`.`product_dim`.`type` AS `Product Type`,
	`warehouse`.`product_dim`.`color` AS `Product Color`,
	`warehouse`.`product_dim`.`consumer` AS `Product Consumer`,
	(`warehouse`.`facts`.`quantity` * `warehouse`.`product_dim`.`price`) AS `Total Cost`,
	`warehouse`.`shop_dim`.`name` AS `Market`,
	`warehouse`.`date_dim`.`month` AS `Month`,
	`warehouse`.`date_dim`.`year` AS `Year` 
from ((((`warehouse`.`facts`  join `warehouse`.`date_dim` on((`warehouse`.`facts`.`date` = `warehouse`.`date_dim`.`id`))) join `warehouse`.`product_dim` on((`warehouse`.`facts`.`product` = `warehouse`.`product_dim`.`id`))) join `warehouse`.`shop_dim` on((`warehouse`.`facts`.`shop` = `warehouse`.`shop_dim`.`id`))))


