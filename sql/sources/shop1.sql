CREATE DATABASE 'shop1';

CREATE TABLE 'sellers' (
  'id' int(11) NOT NULL AUTO_INCREMENT,
  'name' varchar(255) NOT NULL,
  'gender' char(1) NOT NULL,
  PRIMARY KEY ('id')
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

CREATE TABLE 'products' (
  'id' int(11) NOT NULL AUTO_INCREMENT,
  'article' varchar(255) NOT NULL,
  'name' varchar(255) NOT NULL,
  'price' double NOT NULL,
  PRIMARY KEY ('id')
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

CREATE TABLE 'sellings' (
  'id' int(11) NOT NULL AUTO_INCREMENT,
  'bill' varchar(45) NOT NULL,
  'product' int(11) NOT NULL,
  'seller' int(11) NOT NULL,
  'quantity' int(11) NOT NULL,
  'date' date NOT NULL,
  PRIMARY KEY ('id'),
  KEY 'fk_sellings_1' ('product'),
  KEY 'fk_sellings_2' ('seller'),
  CONSTRAINT 'fk_sellings_1' FOREIGN KEY ('product') REFERENCES 'products' ('id') ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT 'fk_sellings_2' FOREIGN KEY ('seller') REFERENCES 'sellers' ('id') ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

-- VIEW FOR shop1 --
CREATE ALGORITHM=UNDEFINED DEFINER='<user>'@'host' SQL SECURITY DEFINER VIEW 'shop1'.'shop1' AS
  SELECT
    'products'.'article' AS 'article',
    'products'.'name' AS 'name',
    'sellings'.'bill' AS 'bill',
    'sellings'.'date' AS 'date',
    'sellings'.'quantity' AS 'quantity'
  FROM 'sellings'
    JOIN 'products' ON
      'sellings'.'product' = 'products'.'id'
;
