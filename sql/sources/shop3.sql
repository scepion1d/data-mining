CREATE DATABASE 'shop2';

CREATE TABLE 'sellers' (
  'id' int(11) NOT NULL AUTO_INCREMENT,
  'name' varchar(255) NOT NULL,
  'gender' char(1) NOT NULL,
  PRIMARY KEY ('id')
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE 'clothing' (
  'id' int(11) NOT NULL AUTO_INCREMENT,
  'name' varchar(45) NOT NULL,
  'color' varchar(45) NOT NULL,
  'consumer' char(1) NOT NULL,
  'price' double NOT NULL,
  PRIMARY KEY ('id')
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE 'footwear' (
  'id' int(11) NOT NULL AUTO_INCREMENT,
  'name' varchar(45) NOT NULL,
  'color' varchar(45) NOT NULL,
  'consumer' char(1) NOT NULL,
  'price' double NOT NULL,
  PRIMARY KEY ('id')
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE 'haberdashery' (
  'id' int(11) NOT NULL AUTO_INCREMENT,
  'name' varchar(45) NOT NULL,
  'color' varchar(45) NOT NULL,
  'consumer' char(1) NOT NULL,
  'price' double NOT NULL,
  PRIMARY KEY ('id')
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE 'bill' (
  'id' int(11) NOT NULL AUTO_INCREMENT,
  'bill' varchar(255) NOT NULL,
  'date' date NOT NULL,
  PRIMARY KEY ('id')
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE 'clothing_sellings' (
  'id' int(11) NOT NULL AUTO_INCREMENT,
  'bill' int(11) NOT NULL,
  'clothing' int(11) NOT NULL,
  'seller' int(11) NOT NULL,
  'quantity' int(11) NOT NULL,
  PRIMARY KEY ('id'),
  KEY 'fk_clothing_sellings_1' ('clothing'),
  KEY 'fk_clothing_sellings_2' ('seller'),
  KEY 'fk_clothing_sellings_3' ('bill'),
  CONSTRAINT 'fk_clothing_sellings_3' FOREIGN KEY ('bill') REFERENCES 'bill' ('id') ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT 'fk_clothing_sellings_1' FOREIGN KEY ('clothing') REFERENCES 'clothing' ('id') ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT 'fk_clothing_sellings_2' FOREIGN KEY ('seller') REFERENCES 'sellers' ('id') ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE 'footwear_sellings' (
  'id' int(11) NOT NULL AUTO_INCREMENT,
  'bill' int(11) NOT NULL,
  'footwear' int(11) NOT NULL,
  'seller' int(11) NOT NULL,
  'quantity' int(11) NOT NULL,
  PRIMARY KEY ('id'),
  KEY 'fk_footwear_sellings_1' ('bill'),
  KEY 'fk_footwear_sellings_2' ('footwear'),
  KEY 'fk_footwear_sellings_3' ('seller'),
  CONSTRAINT 'fk_footwear_sellings_1' FOREIGN KEY ('bill') REFERENCES 'bill' ('id') ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT 'fk_footwear_sellings_2' FOREIGN KEY ('footwear') REFERENCES 'footwear' ('id') ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT 'fk_footwear_sellings_3' FOREIGN KEY ('seller') REFERENCES 'sellers' ('id') ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE 'haberdashery_sellings' (
  'id' int(11) NOT NULL AUTO_INCREMENT,
  'bill' int(11) NOT NULL,
  'haberdashery' int(11) NOT NULL,
  'seller' int(11) NOT NULL,
  'quantity' int(11) NOT NULL,
  PRIMARY KEY ('id'),
  KEY 'fk_haberdashery_sellings_1' ('haberdashery'),
  KEY 'fk_haberdashery_sellings_2' ('seller'),
  KEY 'fk_haberdashery_sellings_3' ('bill'),
  CONSTRAINT 'fk_haberdashery_sellings_3' FOREIGN KEY ('bill') REFERENCES 'bill' ('id') ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT 'fk_haberdashery_sellings_1' FOREIGN KEY ('haberdashery') REFERENCES 'haberdashery' ('id') ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT 'fk_haberdashery_sellings_2' FOREIGN KEY ('seller') REFERENCES 'sellers' ('id') ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf;


CREATE ALGORITHM=UNDEFINED DEFINER=expert@localhost SQL SECURITY DEFINER VIEW shop3 AS
  SELECT
    IF(clothing_sellings.id > 0, 'clothing', 'clothing') as type,
    clothing.color as color,
    clothing.consumer as consumer,
    bills.name as bill,
    bills.date as date,
    clothing_sellings.quantity as quantity,
    clothing.name as name,
    clothing.price as price
  FROM clothing_sellings
    JOIN clothing ON
      clothing_sellings.product = clothing.id
    JOIN bills ON
      clothing_sellings.bill = bills.id

  UNION SELECT
    IF(footwear_sellings.id > 0, 'footwear', 'footwear') as type,
    footwear.color as color,
    footwear.consumer as consumer,
    bills.name as bill,
    bills.date as date,
    footwear_sellings.quantity as quantity,
    footwear.name as name,
    footwear.price as price
   FROM footwear_sellings
    JOIN footwear ON
      footwear_sellings.product = footwear.id
    JOIN bills ON
      footwear_sellings.bill = bills.id

  UNION SELECT
    IF(haberdashery_sellings.id > 0, 'haberdashery', 'haberdashery') as type,
    haberdashery.color as color,
    haberdashery.consumer as consumer,
    bills.name as bill,
    bills.date as date,
    haberdashery_sellings.quantity as quantity,
    haberdashery.name as name,
    haberdashery.price as price
  FROM haberdashery_sellings
    JOIN haberdashery ON
      haberdashery_sellings.product = haberdashery.id
    JOIN bills ON
      haberdashery_sellings.bill = bills.id
;
