CREATE DATABASE 'shop2';

CREATE TABLE 'sellers' (
  'id' int(11) NOT NULL AUTO_INCREMENT,
  'name' varchar(255) NOT NULL,
  'gender' char(1) NOT NULL,
  PRIMARY KEY ('id')
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

CREATE TABLE 'sellings' (
  'id' int(11) NOT NULL AUTO_INCREMENT,
  'bill' varchar(255) NOT NULL,
  'prod_name' varchar(255) NOT NULL,
  'prod_type' varchar(255) NOT NULL,
  'prod_consumer' varchar(255) NOT NULL,
  'prod_color' varchar(255) NOT NULL,
  'prod_price' varchar(255) NOT NULL,
  'seller' int(11) NOT NULL,
  'quantity' int(11) NOT NULL,
  'date' date NOT NULL,
  PRIMARY KEY ('id'),
  KEY 'fk_sellings_1' ('seller'),
  CONSTRAINT 'fk_sellings_1' FOREIGN KEY ('seller') REFERENCES 'sellers' ('id') ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

-- VIEW FOR shop2 --
CREATE ALGORITHM=UNDEFINED DEFINER='<user>'@'<host>' SQL SECURITY DEFINER VIEW 'shop2' AS
  SELECT
    'sellings'.'prod_type' AS 'type',
    'sellings'.'prod_consumer' AS 'consumer',
    'sellings'.'prod_color' AS 'color',
    'sellings'.'bill' AS 'bill',
    'sellings'.'date' AS 'date',
    'sellings'.'quantity' AS 'quantity',
    'sellings'.'prod_name' AS 'name'
  FROM 'sellings'
;